#ifndef CHEESY_SOFTWARE_HOMEBREW_MANAGER_GUI_MONSTER_EDITOR_H
#define CHEESY_SOFTWARE_HOMEBREW_MANAGER_GUI_MONSTER_EDITOR_H

#include <memory>
#include <map>

#include "ui_MonsterEditor.h"

#include "EditorWidget.h"
#include "OutputManager.h"

#include "Monster.h"

namespace HM = CheesySoftware::HomebrewManager;

class MonsterEditor : public EditorWidget
{
  Q_OBJECT

  private:
    //private types
    struct ModWidgets
    {
      QCheckBox* autoCalc;
      QCheckBox* proficient;
      QSpinBox* modifier;
    };

    //member data
    std::unique_ptr<Ui_MonsterEditor> m_ui;
    std::map<HM::Monster::Stat, std::pair<QSpinBox*, QLabel*>> m_statWidgets;
    std::map<HM::Monster::Stat, ModWidgets> m_savingThrowWidgets;
    std::map<HM::Monster::Skill, ModWidgets> m_skillWidgets;

    //private fucntions
    void makeConnections();
    std::int64_t calcModifier(std::int64_t stat, bool proficient) const;
    void updateModInfo(HM::Monster::Stat stat, ModWidgets& modW);

  protected:
    //EditorWidget overrides
    virtual std::shared_ptr<HM::HomebrewItem> fromGui() override;
    virtual void toGui(const std::shared_ptr<HM::HomebrewItem>& item) override;
    virtual std::shared_ptr<HM::HomebrewItem> getDefault() override;

  public:
    //constructors
    MonsterEditor(std::shared_ptr<cheeto::OutputManager> log, QWidget* parent = nullptr);
    MonsterEditor(std::shared_ptr<cheeto::OutputManager> log, std::string filePath,
                  QWidget* parent = nullptr);

    //EditorWidget overrides
    virtual bool unsavedChanges() override;

  public slots:
    void statScoreUpdated(HM::Monster::Stat stat, int newScore);
    void savingThrowAutoUpdated(HM::Monster::Stat stat, bool autoUpdate);
    void savingThrowProfUpdated(HM::Monster::Stat stat);
    void skillAutoUpdated(HM::Monster::Skill skill, bool autoUpdate);
    void skillProfUpdated(HM::Monster::Skill skill);
    void profBonusUpdated();

};

#endif
