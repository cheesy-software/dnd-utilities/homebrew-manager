#ifndef CHEESY_SOFTWARE_HOMEBREW_MANAGER_GUI_RACE_EDITOR_H
#define CHEESY_SOFTWARE_HOMEBREW_MANAGER_GUI_RACE_EDITOR_H

#include <memory>

#include <QCloseEvent>

#include "ui_RaceEditor.h"

class RaceEditor : public QMainWindow
{
  Q_OBJECT

  private:
    //member data
    std::unique_ptr<Ui_RaceEditor> m_ui;

  protected:
    //event handlers
    void closeEvent(QCloseEvent* e) override;

  public:
    //constructors
    RaceEditor(QWidget* parent = nullptr);

  signals:
    void closing();

  public slots:

};

#endif
