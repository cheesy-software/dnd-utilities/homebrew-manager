#ifndef CHEESY_SOFTWARE_HOMEBREW_MANAGER_GUI_FEAT_EDITOR_H
#define CHEESY_SOFTWARE_HOMEBREW_MANAGER_GUI_FEAT_EDITOR_H

#include <memory>

#include "ui_FeatEditor.h"

#include "EditorWidget.h"

namespace HM = CheesySoftware::HomebrewManager;

class FeatEditor : public EditorWidget
{
  Q_OBJECT

  private:
    //member data
    std::unique_ptr<Ui_FeatEditor> m_ui;

    //private functions
    void makeConnections();

  protected:
    //EditorWidget overrides
    virtual std::shared_ptr<HM::HomebrewItem> fromGui() override;
    virtual void toGui(const std::shared_ptr<HM::HomebrewItem>&) override;
    virtual std::shared_ptr<HM::HomebrewItem> getDefault() override;

  public:
    //constructors
    FeatEditor(std::shared_ptr<cheeto::OutputManager> log, QWidget* parent = nullptr);
    FeatEditor(std::shared_ptr<cheeto::OutputManager> log, std::string filePath,
               QWidget* parent = nullptr);

    //EditorWidget overrides
    virtual bool unsavedChanges() override;

  public slots:

};

#endif
