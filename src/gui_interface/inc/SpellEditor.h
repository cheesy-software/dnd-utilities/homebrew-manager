#ifndef CHEESY_SOFTWARE_HOMEBREW_MANAGER_GUI_SPELL_EDITOR_H
#define CHEESY_SOFTWARE_HOMEBREW_MANAGER_GUI_SPELL_EDITOR_H

#include <memory>

#include "ui_SpellEditor.h"

#include "EditorWidget.h"
#include "OutputManager.h"

namespace HM = CheesySoftware::HomebrewManager;

class SpellEditor : public EditorWidget
{
  Q_OBJECT

  private:
    //member data
    std::unique_ptr<Ui_SpellEditor> m_ui;

    //private functions
    void makeConnections();

  protected:
    //EditorWdiget overrides
    virtual std::shared_ptr<HM::HomebrewItem> fromGui() override;
    virtual void toGui(const std::shared_ptr<HM::HomebrewItem>& item) override;
    virtual std::shared_ptr<HM::HomebrewItem> getDefault() override;

  public:
    //constructors
    SpellEditor(std::shared_ptr<cheeto::OutputManager> log, QWidget* parent = nullptr);
    SpellEditor(std::shared_ptr<cheeto::OutputManager> log, std::string filePath,
                QWidget* parent = nullptr);

    //EditorWidget overrides
    virtual bool unsavedChanges() override;

  public slots:
    void chkMaterialHandler(bool checked);
};

#endif
