#ifndef CHEESY_SOFTWARE_HOMEBREW_MANAGER_GUI_SELECTION_SCREEN_H
#define CHEESY_SOFTWARE_HOMEBREW_MANAGER_GUI_SELECTION_SCREEN_H

//std includes
#include <memory>

//GUI include
#include "ui_SelectionScreen.h"

//custom includes
#include "OutputManager.h"
#include "ApplicationOptions.h"
#include "HomebrewWindow.h"

namespace HM = CheesySoftware::HomebrewManager;

class SelectionScreen : public QMainWindow
{
  private:
    //member data
    std::unique_ptr<Ui_SelectionScreen> m_ui;
    std::shared_ptr<cheeto::OutputManager> m_log;
    HM::AppOptions m_options;

    //private functions
    void makeConnections();
    template <typename EditorWindow, typename... Args>
    void openEditor(Args... args)
    {
      (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SelectionScreen: openEditor()\n";
      auto win = new HomebrewWindow(new EditorWindow(args...), m_log, this);
      connect(win, &HomebrewWindow::closing, [this]() { this->show(); });
      win->show();
      this->hide();
    }

  public:
    //constructors
    SelectionScreen(const HM::AppOptions& options, QWidget* parent = nullptr);

  signals:

  public slots:
    void btnClassEditorHandler();
    void btnEncounterEditorHandler();
    void btnFeatEditorHandler();
    void btnItemEditorHandler();
    void btnMonsterEditorHandler();
    void btnNpcEditorHandler();
    void btnRaceEditorHandler();
    void btnSpellEditorHandler();
    void actClassEditorHandler();
    void actEncounterEditorHandler();
    void actFeatEditorHandler();
    void actItemEditorHandler();
    void actMonsterEditorHandler();
    void actNpcEditorHandler();
    void actRaceEditorHandler();
    void actSpellEditorHandler();
    void actSettingsHandler();
    void actAboutHandler();
    void actAboutQtHandler();

};

#endif
