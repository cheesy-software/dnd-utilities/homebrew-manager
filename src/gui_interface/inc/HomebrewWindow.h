#ifndef CHEESY_SOFTWARE_HOMEBREW_MANAGER_HOMEBREW_WINDOW_H
#define CHEESY_SOFTWARE_HOMEBREW_MANAGER_HOMEBREW_WINDOW_H

#include <memory>

#include <QMainWindow>
#include <QCloseEvent>
#include <QVBoxLayout>
#include <QPushButton>
#include <QMenuBar>
#include <QMenu>

#include "ApplicationOptions.h"
#include "OutputManager.h"
#include "EditorWidget.h"

namespace HM = CheesySoftware::HomebrewManager;

struct Ui_HomebrewWindow;

class HomebrewWindow : public QMainWindow
{
  Q_OBJECT

  private:
    //std::unique_ptr<Ui_HomebrewWindow> m_ui;
    std::shared_ptr<Ui_HomebrewWindow> m_ui;
    std::shared_ptr<cheeto::OutputManager> m_log;

    //private functions
    void makeConnections();
    bool confirmWorkLoss();
    void save();
    void load();
    void clear();

  protected:
    //event handlers
    void closeEvent(QCloseEvent* e) override;

  public:
    //constructors
    HomebrewWindow(EditorWidget* centralWidget, std::shared_ptr<cheeto::OutputManager> log,
                   QWidget* parent = nullptr);

  signals:
    void closing();

  public slots:
    void btnSaveHandler();
    void actNewHandler();
    void actOpenHandler();
    void actSaveHandler();
    void actQuitHandler();
};

#endif
