#ifndef CHEESY_SOFTWARE_HOMEBREW_MANAGER_GUI_ENCOUNTER_EDITOR_H
#define CHEESY_SOFTWARE_HOMEBREW_MANAGER_GUI_ENCOUNTER_EDITOR_H

#include <memory>

#include <QCloseEvent>

#include "ui_EncounterEditor.h"

class EncounterEditor : public QMainWindow
{
  Q_OBJECT

  private:
    //member data
    std::unique_ptr<Ui_EncounterEditor> m_ui;

  protected:
    //event handlers
    void closeEvent(QCloseEvent* e) override;

  public:
    //constructors
    EncounterEditor(QWidget* parent = nullptr);

  signals:
    void closing();

  public slots:

};

#endif
