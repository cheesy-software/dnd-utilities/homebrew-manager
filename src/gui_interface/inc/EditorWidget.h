#ifndef CHEESY_SOFTWARE_HOMEBREW_MANAGER_EDITOR_WIDGET_H
#define CHEESY_SOFTWARE_HOMEBREW_MANAGER_EDITOR_WIDGET_H

//TODO make appropriate functions const

#include <memory>

#include <QWidget>

#include "OutputManager.h"
#include "ApplicationOptions.h"
#include "HomebrewItem.h"

namespace HM = CheesySoftware::HomebrewManager;

//template <typename T>
class EditorWidget : public QWidget
{
  protected:
    //protected data members
    std::shared_ptr<HM::HomebrewItem> m_lastSavedVersion;
    std::shared_ptr<cheeto::OutputManager> m_log;

    //protected functions
    virtual std::shared_ptr<HM::HomebrewItem> fromGui() = 0;
    virtual void toGui(const std::shared_ptr<HM::HomebrewItem>&) = 0;
    virtual std::shared_ptr<HM::HomebrewItem> getDefault() = 0;

    //constructors
    EditorWidget(std::shared_ptr<cheeto::OutputManager> log, QWidget* parent = nullptr);

  public:
    virtual ~EditorWidget() = default;
    virtual bool unsavedChanges() = 0;

  public slots:
    virtual void saveFile(const QString& filePath);
    virtual void loadFile(const QString& filePath);
    virtual void clearGui();
};

#endif
