#ifndef CHEESY_SOFTWARE_HOMEBREW_MANAGER_GUI_ITEM_EDITOR_H
#define CHEESY_SOFTWARE_HOMEBREW_MANAGER_GUI_ITEM_EDITOR_H

#include <memory>

#include <QCloseEvent>

#include "ui_ItemEditor.h"

class ItemEditor : public QMainWindow
{
  Q_OBJECT

  private:
    //member data
    std::unique_ptr<Ui_ItemEditor> m_ui;

  protected:
    //event handlers
    void closeEvent(QCloseEvent* e) override;

  public:
    //constructors
    ItemEditor(QWidget* parent = nullptr);

  signals:
    void closing();

  public slots:

};

#endif
