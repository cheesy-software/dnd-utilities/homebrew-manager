#ifndef CHEESY_SOFTWARE_HOMEBREW_MANAGER_GUI_NPC_EDITOR_H
#define CHEESY_SOFTWARE_HOMEBREW_MANAGER_GUI_NPC_EDITOR_H

#include <memory>

#include <QCloseEvent>

#include "ui_NpcEditor.h"

class NpcEditor : public QMainWindow
{
  Q_OBJECT

  private:
    //member data
    std::unique_ptr<Ui_NpcEditor> m_ui;

  protected:
    //event handlers
    void closeEvent(QCloseEvent* e) override;

  public:
    //constructors
    NpcEditor(QWidget* parent = nullptr);

  signals:
    void closing();

  public slots:

};

#endif
