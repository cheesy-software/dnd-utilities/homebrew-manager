#include "SpellEditor.h"

#include "ApplicationOptions.h"
#include "Spell.h"

//private functions
void SpellEditor::makeConnections()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SpellEditor: makeConnections()\n";

  connect(m_ui->chkMaterial, &QCheckBox::toggled, this, &SpellEditor::chkMaterialHandler);
}

std::shared_ptr<HM::HomebrewItem> SpellEditor::fromGui()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SpellEditor: fromGui()\n";
  auto s = std::make_unique<HM::Spell>();

  s->name = m_ui->lneSpellName->text().toStdString();
  s->level = m_ui->spnLevel->value();
  s->castingTime = m_ui->lneCastingTime->text().toStdString();
  s->range = m_ui->lneRange->text().toStdString();
  s->duration = m_ui->lneDuration->text().toStdString();
  s->concentration = m_ui->chkConcentration->isChecked();
  s->ritual = m_ui->chkRitual->isChecked();
  s->effect = m_ui->txtSpellEffect->toPlainText().toStdString();
  s->upcastEffect = m_ui->txtUpcastEffect->toPlainText().toStdString();
  s->school = (HM::Spell::School)m_ui->cmbSchool->currentIndex();

  //components
  s->components.verbal = m_ui->chkVerbal->isChecked();
  s->components.somatic = m_ui->chkSomatic->isChecked();
  s->components.material = m_ui->chkMaterial->isChecked();
  if (s->components.material)
  {
    s->components.materialComponents = m_ui->lneMaterials->text().toStdString();
  } //end  if (s.components.material)

  //classses
  if (m_ui->chkBard->isChecked())
  {
    s->classes.insert(HM::Spell::Class::BARD);
  } //end  if (m_ui->chkBard->isChecked())
  if (m_ui->chkCleric->isChecked())
  {
    s->classes.insert(HM::Spell::Class::CLERIC);
  } //end  if (m_ui->chkCleric->isChecked())
  if (m_ui->chkDruid->isChecked())
  {
    s->classes.insert(HM::Spell::Class::DRUID);
  } //end  if (m_ui->chkDruid->isChecked())
  if (m_ui->chkPaladin->isChecked())
  {
    s->classes.insert(HM::Spell::Class::PALADIN);
  } //end  if (m_ui->chkPaladin->isChecked())
  if (m_ui->chkRanger->isChecked())
  {
    s->classes.insert(HM::Spell::Class::RANGER);
  } //end  if (m_ui->chkRanger->isChecked())
  if (m_ui->chkSorcerer->isChecked())
  {
    s->classes.insert(HM::Spell::Class::SORCERER);
  } //end  if (m_ui->chkSorcerer->isChecked())
  if (m_ui->chkWarlock->isChecked())
  {
    s->classes.insert(HM::Spell::Class::WARLOCK);
  } //end  if (m_ui->chkWarlock->isChecked())
  if (m_ui->chkWizard->isChecked())
  {
    s->classes.insert(HM::Spell::Class::WIZARD);
  } //end  if (m_ui->chkWizard->isChecked())

  return s;
}
void SpellEditor::toGui(const std::shared_ptr<HM::HomebrewItem>& item)
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SpellEditor: toGui()\n";

  //verify that its the right type
  auto spell = std::dynamic_pointer_cast<HM::Spell>(item);
  if (spell == nullptr)
  {
    (*m_log)((int)HM::Verbosity::ERROR) << "ERROR: SpellEditor: called with non-spell item\n";
  } //end  if (spell == nullptr)

  m_ui->lneSpellName->setText(QString::fromStdString(spell->name));
  m_ui->spnLevel->setValue(spell->level);
  m_ui->lneCastingTime->setText(QString::fromStdString(spell->castingTime));
  m_ui->lneRange->setText(QString::fromStdString(spell->range));
  m_ui->lneDuration->setText(QString::fromStdString(spell->duration));
  m_ui->chkConcentration->setChecked(spell->concentration);
  m_ui->chkRitual->setChecked(spell->ritual);
  m_ui->txtSpellEffect->setPlainText(QString::fromStdString(spell->effect));
  m_ui->txtUpcastEffect->setPlainText(QString::fromStdString(spell->upcastEffect));
  m_ui->cmbSchool->setCurrentIndex((int)spell->school);

  //components
  m_ui->chkVerbal->setChecked(spell->components.verbal);
  m_ui->chkSomatic->setChecked(spell->components.somatic);
  m_ui->chkMaterial->setChecked(spell->components.material);
  if (spell->components.material)
  {
    m_ui->lneMaterials->setText(QString::fromStdString(spell->components.materialComponents));
  } //end  if (s.components.material)
  else
  {
    m_ui->lneMaterials->setText("");
  } //end  else

  //classses
  m_ui->chkBard->setChecked(false);
  m_ui->chkCleric->setChecked(false);
  m_ui->chkDruid->setChecked(false);
  m_ui->chkPaladin->setChecked(false);
  m_ui->chkRanger->setChecked(false);
  m_ui->chkSorcerer->setChecked(false);
  m_ui->chkWarlock->setChecked(false);
  m_ui->chkWizard->setChecked(false);
  for (const auto& playerClass : spell->classes)
  {
    switch(playerClass)
    {
      case HM::Spell::Class::BARD:
        m_ui->chkBard->setChecked(true);
        break;
      case HM::Spell::Class::CLERIC:
        m_ui->chkCleric->setChecked(true);
        break;
      case HM::Spell::Class::DRUID:
        m_ui->chkDruid->setChecked(true);
        break;
      case HM::Spell::Class::PALADIN:
        m_ui->chkPaladin->setChecked(true);
        break;
      case HM::Spell::Class::RANGER:
        m_ui->chkRanger->setChecked(true);
        break;
      case HM::Spell::Class::SORCERER:
        m_ui->chkSorcerer->setChecked(true);
        break;
      case HM::Spell::Class::WARLOCK:
        m_ui->chkWarlock->setChecked(true);
        break;
      case HM::Spell::Class::WIZARD:
        m_ui->chkWizard->setChecked(true);
        break;
    } //end  switch(playerClass)
  } //end  for (const auto& playerClass : spell->classes)
}
std::shared_ptr<HM::HomebrewItem> SpellEditor::getDefault()
{
  return std::make_shared<HM::Spell>();
}

//constructors
SpellEditor::SpellEditor(std::shared_ptr<cheeto::OutputManager> log, QWidget* parent) :
  SpellEditor(log, "", parent)
{
}
SpellEditor::SpellEditor(std::shared_ptr<cheeto::OutputManager> log, std::string filePath,
                         QWidget* parent) :
  EditorWidget(log, parent),
  m_ui(std::make_unique<Ui_SpellEditor>())
{
  //TODO add handling for filePath
  m_ui->setupUi(this);

  //populate the school combobox
  for (const auto& school : HM::Spell::SCHOOL_STR_MAP)
  {
    m_ui->cmbSchool->addItem(QString::fromStdString(school.second));
  } //end  for (const auto& school: HM::Spell::SCHOOL_STR_MAP)

  makeConnections();

  //set GUI to default spell
  clearGui();
}

bool SpellEditor::unsavedChanges()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SpellEditor: unsavedChanges()\n";

  auto lhs = std::dynamic_pointer_cast<HM::Spell>(m_lastSavedVersion);
  auto rhs = std::dynamic_pointer_cast<HM::Spell>(fromGui());

  return *lhs == *rhs;
}

void SpellEditor::chkMaterialHandler(bool checked)
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SpellEditor: chkMaterialHandler()\n";
  m_ui->lblMaterials->setEnabled(checked);
  m_ui->lneMaterials->setEnabled(checked);
}

