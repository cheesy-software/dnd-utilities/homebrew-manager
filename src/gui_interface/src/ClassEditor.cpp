#include "ClassEditor.h"

//event handlers
void ClassEditor::closeEvent(QCloseEvent* e)
{
  emit closing();

  QMainWindow::closeEvent(e);
}

//constructors
ClassEditor::ClassEditor(QWidget* parent) :
  m_ui(std::make_unique<Ui_ClassEditor>())
{
  m_ui->setupUi(this);
}

