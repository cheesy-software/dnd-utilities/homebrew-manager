#include "NpcEditor.h"

//event handlers
void NpcEditor::closeEvent(QCloseEvent* e)
{
  emit closing();

  QMainWindow::closeEvent(e);
}

//constructors
NpcEditor::NpcEditor(QWidget* parent) :
  m_ui(std::make_unique<Ui_NpcEditor>())
{
  m_ui->setupUi(this);
}

