#include "RaceEditor.h"

//event handlers
void RaceEditor::closeEvent(QCloseEvent* e)
{
  emit closing();

  QMainWindow::closeEvent(e);
}

//constructors
RaceEditor::RaceEditor(QWidget* parent) :
  m_ui(std::make_unique<Ui_RaceEditor>())
{
  m_ui->setupUi(this);
}

