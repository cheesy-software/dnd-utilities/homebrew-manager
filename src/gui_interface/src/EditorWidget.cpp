#include "EditorWidget.h"

//protected constructor
EditorWidget::EditorWidget(std::shared_ptr<cheeto::OutputManager> log, QWidget* parent) :
  QWidget(parent),
  m_log(log)
{
}

//public functions

//public slots
void EditorWidget::saveFile(const QString& filePath)
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: EditorWidget: saveFile()\n";

  //save the file
  auto item = fromGui();
  if (!item->saveToFile(filePath.toStdString()))
  {
    (*m_log)((int)HM::Verbosity::ERROR) << "ERROR: EditorWidget: failed to save to file\n";
  } //end  if (!HM::Spell::saveToFile(filePath.toStdString(), s))

  //update most recently saved version
  m_lastSavedVersion = item;
}
void EditorWidget::loadFile(const QString& filePath)
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: EditorWidget: loadFile()\n";

  //load spell from file into memory
  //auto item = std::make_shared<HM::HomebrewItem>();
  auto item = getDefault();
  if (!item->loadFromFile(filePath.toStdString()))
  {
    (*m_log)((int)HM::Verbosity::ERROR) << "ERROR: EditorWidget: Failed to load: " << filePath.toStdString() << "\n";
    return;
  } //end  if (!HM::Spell::loadFromFile(fileName.toStdString(), s))

  //apply to GUI
  toGui(item);

  //update last saved to what was loaded
  m_lastSavedVersion = item;
}
void EditorWidget::clearGui()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: EditorWidget: clearGUI()\n";

  //clear all GUI elements
  auto defaultItem = getDefault();
  toGui(defaultItem);

  //update saved spell
  m_lastSavedVersion = defaultItem;
}
