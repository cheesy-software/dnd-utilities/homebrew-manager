#include "EncounterEditor.h"

//event handlers
void EncounterEditor::closeEvent(QCloseEvent* e)
{
  emit closing();

  QMainWindow::closeEvent(e);
}

//constructors
EncounterEditor::EncounterEditor(QWidget* parent) :
  m_ui(std::make_unique<Ui_EncounterEditor>())
{
  m_ui->setupUi(this);
}

