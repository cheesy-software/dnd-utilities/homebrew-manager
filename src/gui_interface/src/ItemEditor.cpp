#include "ItemEditor.h"

//event handlers
void ItemEditor::closeEvent(QCloseEvent* e)
{
  emit closing();

  QMainWindow::closeEvent(e);
}

//constructors
ItemEditor::ItemEditor(QWidget* parent) :
  m_ui(std::make_unique<Ui_ItemEditor>())
{
  m_ui->setupUi(this);
}

