#include "SelectionScreen.h"

#include <QMessageBox>

#include "ClassEditor.h"
#include "EncounterEditor.h"
#include "FeatEditor.h"
#include "ItemEditor.h"
#include "MonsterEditor.h"
#include "NpcEditor.h"
#include "RaceEditor.h"
#include "SpellEditor.h"

//private functions
void SelectionScreen::makeConnections()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SelectionScreen: makeConnections()\n";

  //button connections
  connect(m_ui->btnClassEditor, &QPushButton::clicked, this, &SelectionScreen::btnClassEditorHandler);
  connect(m_ui->btnEncounterEditor, &QPushButton::clicked, this, &SelectionScreen::btnEncounterEditorHandler);
  connect(m_ui->btnFeatEditor, &QPushButton::clicked, this, &SelectionScreen::btnFeatEditorHandler);
  connect(m_ui->btnItemEditor, &QPushButton::clicked, this, &SelectionScreen::btnItemEditorHandler);
  connect(m_ui->btnMonsterEditor, &QPushButton::clicked, this, &SelectionScreen::btnMonsterEditorHandler);
  connect(m_ui->btnNpcEditor, &QPushButton::clicked, this, &SelectionScreen::btnNpcEditorHandler);
  connect(m_ui->btnRaceEditor, &QPushButton::clicked, this, &SelectionScreen::btnRaceEditorHandler);
  connect(m_ui->btnSpellEditor, &QPushButton::clicked, this, &SelectionScreen::btnSpellEditorHandler);

  //menu action connections
  connect(m_ui->actClassEditor, &QAction::triggered, this, &SelectionScreen::actClassEditorHandler);
  connect(m_ui->actEncounterEditor, &QAction::triggered, this, &SelectionScreen::actEncounterEditorHandler);
  connect(m_ui->actFeatEditor, &QAction::triggered, this, &SelectionScreen::actFeatEditorHandler);
  connect(m_ui->actItemEditor, &QAction::triggered, this, &SelectionScreen::actItemEditorHandler);
  connect(m_ui->actMonsterEditor, &QAction::triggered, this, &SelectionScreen::actMonsterEditorHandler);
  connect(m_ui->actNpcEditor, &QAction::triggered, this, &SelectionScreen::actNpcEditorHandler);
  connect(m_ui->actRaceEditor, &QAction::triggered, this, &SelectionScreen::actRaceEditorHandler);
  connect(m_ui->actSpellEditor, &QAction::triggered, this, &SelectionScreen::actSpellEditorHandler);
  connect(m_ui->actSettings, &QAction::triggered, this, &SelectionScreen::actSettingsHandler);
  connect(m_ui->actAbout, &QAction::triggered, this, &SelectionScreen::actAboutHandler);
  connect(m_ui->actAboutQt, &QAction::triggered, this, &SelectionScreen::actAboutQtHandler);
}

//constructors
SelectionScreen::SelectionScreen(const HM::AppOptions& options, QWidget* parent) :
  QMainWindow(parent),
  m_ui(std::make_unique<Ui_SelectionScreen>()),
  m_log(std::make_shared<cheeto::OutputManager>()),
  m_options(options)
{
  //setup GUI
  m_ui->setupUi(this);

  //setup output manager
  m_log->addOutputLocation("out", std::cout, (int)m_options.verbosity, std::chrono::milliseconds(200), "\n");
  m_log->addFile("log", m_options.logPath, (int)HM::Verbosity::ALL_INFO, std::chrono::milliseconds(200), "\n");

  //print options used TODO fix not being able to print the enums out
  (*m_log)((int)HM::Verbosity::VERBOSE_INFO) << "Verbosity: " << (int)m_options.verbosity << "\n";
  (*m_log)((int)HM::Verbosity::VERBOSE_INFO) << "Log path: " << m_options.logPath << "\n";
  (*m_log)((int)HM::Verbosity::VERBOSE_INFO) << "Editor: " << (int)m_options.editor << "\n";

  makeConnections();

  //if provided, open directly to an editor
  switch(m_options.editor)
  {
    case HM::Editor::CLASS:
      //openEditor<ClassEditor>(this);
      break;
    case HM::Editor::ENCOUNTER:
      //openEditor<EncounterEditor>(this);
      break;
    case HM::Editor::FEAT:
      openEditor<FeatEditor>(m_log, this);
      break;
    case HM::Editor::ITEM:
      //openEditor<ItemEditor>(this);
      break;
    case HM::Editor::MONSTER:
      openEditor<MonsterEditor>(m_log, this);
      break;
    case HM::Editor::NPC:
      //openEditor<NpcEditor>(this);
      break;
    case HM::Editor::RACE:
      //openEditor<RaceEditor>(this);
      break;
    case HM::Editor::SPELL:
      openEditor<SpellEditor>(m_log, this);
      break;
    default:
      break;
  }
}

//public slots
void SelectionScreen::btnClassEditorHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SelectionScreen: btnClassEditorHandler()\n";
  //openEditor<ClassEditor>(this);
}
void SelectionScreen::btnEncounterEditorHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SelectionScreen: btnEncounterEditorHandler()\n";
  //openEditor<EncounterEditor>(this);
}
void SelectionScreen::btnFeatEditorHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SelectionScreen: btnFeatEditorHandler()\n";
  openEditor<FeatEditor>(m_log, this);
}
void SelectionScreen::btnItemEditorHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SelectionScreen: btnItemEditorHandler()\n";
  //openEditor<ItemEditor>(this);
}
void SelectionScreen::btnMonsterEditorHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SelectionScreen: btnMonsterEditorHandler()\n";
  openEditor<MonsterEditor>(m_log, this);
}
void SelectionScreen::btnNpcEditorHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SelectionScreen: btnNpcEditorHandler()\n";
  //openEditor<NpcEditor>(this);
}
void SelectionScreen::btnRaceEditorHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SelectionScreen: btnRaceEditorHandler()\n";
  //openEditor<RaceEditor>(this);
}
void SelectionScreen::btnSpellEditorHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SelectionScreen: btnSpellEditorHandler()\n";
  openEditor<SpellEditor>(m_log, this);
}
void SelectionScreen::actClassEditorHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SelectionScreen: actClassEditorHandler()\n";
  //openEditor<ClassEditor>(this);
}
void SelectionScreen::actEncounterEditorHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SelectionScreen: actEncounterEditorHandler()\n";
  //openEditor<EncounterEditor>(this);
}
void SelectionScreen::actFeatEditorHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SelectionScreen: actFeatEditorHandler()\n";
  openEditor<FeatEditor>(m_log, this);
}
void SelectionScreen::actItemEditorHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SelectionScreen: actItemEditorHandler()\n";
  //openEditor<ItemEditor>(this);
}
void SelectionScreen::actMonsterEditorHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SelectionScreen: actMonsterEditorHandler()\n";
  openEditor<MonsterEditor>(m_log, this);
}
void SelectionScreen::actNpcEditorHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SelectionScreen: actNpcEditorHandler()\n";
  //openEditor<NpcEditor>(this);
}
void SelectionScreen::actRaceEditorHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SelectionScreen: actRaceEditorHandler()\n";
  //openEditor<RaceEditor>(this);
}
void SelectionScreen::actSpellEditorHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SelectionScreen: actSpellEditorHandler()\n";
  openEditor<SpellEditor>(m_log, this);
}
void SelectionScreen::actSettingsHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SelectionScreen: actSettingsHandler()\n";
}
void SelectionScreen::actAboutHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SelectionScreen: actAboutHandler()\n";
  QMessageBox box;
  box.setText(QCoreApplication::applicationName());
  box.setInformativeText("version: " + QCoreApplication::applicationVersion() + "\n"
                         "Author: " + QCoreApplication::organizationName() + "\n"
                        );
  box.exec();
}
void SelectionScreen::actAboutQtHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: SelectionScreen: actAboutHandler()\n";
  QMessageBox::aboutQt(this);
}

