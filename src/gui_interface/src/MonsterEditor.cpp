#include "MonsterEditor.h"

#include <functional>
#include <cmath>

#include "ApplicationOptions.h"

//private functions
void MonsterEditor::makeConnections()
{
  //stat widget connections
  for (auto& widgets : m_statWidgets)
  {
    connect(widgets.second.first, QOverload<int>::of(&QSpinBox::valueChanged), [this, stat = widgets.first](int newScore) {
        this->statScoreUpdated(stat, newScore);
      });
  } //end  for (auto& widgets : m_statWidgets)

  //saving throw widget connections
  for (auto& widgets : m_savingThrowWidgets)
  {
    connect(widgets.second.autoCalc, &QCheckBox::toggled, [this, stat = widgets.first](bool value) {
        this->savingThrowAutoUpdated(stat, value);
      });
    connect(widgets.second.proficient, &QCheckBox::toggled, [this, stat = widgets.first] () {
        this->savingThrowProfUpdated(stat);
      });
  } //end  for (auto& widgets : m_savingThrowWidgets)

  //skill widget connections
  for (auto& widgets : m_skillWidgets)
  {
    connect(widgets.second.autoCalc, &QCheckBox::toggled, [this, skill = widgets.first] (bool value) {
        this->skillAutoUpdated(skill, value);
      });
    connect(widgets.second.proficient, &QCheckBox::toggled, [this, skill = widgets.first] () {
        this->skillProfUpdated(skill);
      });
  } //end  for (auto& widgets : m_skillWidgets)

  connect(m_ui->spnProficiencyBonus, QOverload<int>::of(&QSpinBox::valueChanged), this, &MonsterEditor::profBonusUpdated);
}
std::int64_t MonsterEditor::calcModifier(std::int64_t stat, bool proficient) const
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: MonsterEditor: calcModifier()\n";

  auto mod = std::floor(stat / 2) - 5;
  if (proficient)
  {
    mod += m_ui->spnProficiencyBonus->value();
  } //end  if (proficient)

  (*m_log)((int)HM::Verbosity::DEBUG_INFO) << "DEBUG: MonsterEditor: score: " << stat
                                           << " mod: " << mod << "\n";

  return mod;
}
void MonsterEditor::updateModInfo(HM::Monster::Stat stat, ModWidgets& modW)
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: MonsterEditor: updateModInfo()\n";

  if (modW.autoCalc->isChecked())
  {
    (*m_log)((int)HM::Verbosity::DEBUG_INFO) << "DEBUG: MonsterEditor: auto update enabled, calculating modifier\n";
    const auto& statWidgets = m_statWidgets[stat];
    auto statScore = statWidgets.first->value();
    auto prof = modW.proficient->isChecked();
    auto modifier = calcModifier(statScore, prof);

    (*m_log)((int)HM::Verbosity::DEBUG_INFO) << "DEBUG: MonsterEditor: "
                                             << "stat: " << HM::Monster::STAT_STR_MAP.at(stat) << " | "
                                             << "statScore: " << statScore << " | "
                                             << "proficient: " << prof << " | "
                                             << "modifier: " << modifier << "\n";

    modW.modifier->setValue(modifier);;
  } //end  if (modW.autoCalc->isChecked())
}

//EditorWidget overrides
std::shared_ptr<HM::HomebrewItem> MonsterEditor::fromGui()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: MonsterEditor: fromGui()\n";
  auto m = std::make_shared<HM::Monster>();

  m->name = m_ui->lneName->text().toStdString();
  m->size = (HM::Monster::Size)m_ui->cmbSize->currentIndex();
  m->type = (HM::Monster::Type)m_ui->cmbType->currentIndex();
  m->alignment = (HM::Monster::Alignment)m_ui->cmbAlignment->currentIndex();
  m->ac = m_ui->spnAc->value();
  m->acReason = m_ui->lneAcReason->text().toStdString();
  m->hp = m_ui->spnHp->value();
  m->hpDice = m_ui->lneHpDice->text().toStdString();
  m->speed = m_ui->lneSpeed->text().toStdString();
  m->senses = m_ui->lneSenses->text().toStdString();
  m->languages = m_ui->lneLanguages->text().toStdString();
  m->challengeRating = m_ui->lneCr->text().toStdString();
  m->xp = m_ui->spnXp->value();
  m->abilities = m_ui->pteAbilities->toPlainText().toStdString();
  m->actions = m_ui->pteActions->toPlainText().toStdString();
  m->info = m_ui->pteInfo->toPlainText().toStdString();
  m->proficiencyBonus = m_ui->spnProficiencyBonus->value();
  m->damageResistances = m_ui->lneDamageResistance->text().toStdString();
  m->damageImmunities = m_ui->lneDamageImmunity->text().toStdString();
  m->damageVulnerabilities = m_ui->lneDamageVulnerability->text().toStdString();
  m->conditionImmunities = m_ui->lneConditionImmunity->text().toStdString();

  //stats
  for (const auto& s : m_statWidgets)
  {
    m->stats[s.first] = s.second.first->value();
  } //end  for (const auto& s : m_statWidgets)

  //saving throws
  for (const auto& st : m_savingThrowWidgets)
  {
    HM::Monster::ModifierInfo modInfo;

    modInfo.autoCalculate = st.second.autoCalc->isChecked();
    modInfo.proficient = st.second.proficient->isChecked();
    modInfo.modifier = st.second.modifier->value();

    m->savingThrows[st.first] = modInfo;
  } //end  for (const auto& st : m_savingThrowWidgets)

  //skills
  for (const auto& s : m_skillWidgets)
  {
    HM::Monster::ModifierInfo modInfo;

    modInfo.autoCalculate = s.second.autoCalc->isChecked();
    modInfo.proficient = s.second.proficient->isChecked();
    modInfo.modifier = s.second.modifier->value();

    m->skills[s.first] = modInfo;
  } //end  for (const auto& st : m_savingThrowWidgets)

  return m;
}
void MonsterEditor::toGui(const std::shared_ptr<HM::HomebrewItem>& item)
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: MonsterEditor: toGui()\n";

  //verify that its the right type
  auto monster = std::dynamic_pointer_cast<HM::Monster>(item);
  if (monster == nullptr)
  {
    (*m_log)((int)HM::Verbosity::ERROR) << "ERROR: MonsterEditor: toGui called with non-monster item\n";
  }

  m_ui->lneName->setText(QString::fromStdString(monster->name));
  m_ui->cmbSize->setCurrentIndex((int)monster->size);
  m_ui->cmbType->setCurrentIndex((int)monster->type);
  m_ui->cmbAlignment->setCurrentIndex((int)monster->alignment);
  m_ui->spnAc->setValue(monster->ac);
  m_ui->lneAcReason->setText(QString::fromStdString(monster->acReason));
  m_ui->spnHp->setValue(monster->hp);
  m_ui->lneHpDice->setText(QString::fromStdString(monster->hpDice));
  m_ui->lneSpeed->setText(QString::fromStdString(monster->speed));
  m_ui->lneSenses->setText(QString::fromStdString(monster->senses));
  m_ui->lneLanguages->setText(QString::fromStdString(monster->languages));
  m_ui->lneCr->setText(QString::fromStdString(monster->challengeRating));
  m_ui->spnXp->setValue(monster->xp);
  m_ui->pteAbilities->setPlainText(QString::fromStdString(monster->abilities));
  m_ui->pteActions->setPlainText(QString::fromStdString(monster->actions));
  m_ui->pteInfo->setPlainText(QString::fromStdString(monster->info));
  m_ui->spnProficiencyBonus->setValue(monster->proficiencyBonus);
  m_ui->lneDamageResistance->setText(QString::fromStdString(monster->damageResistances));
  m_ui->lneDamageImmunity->setText(QString::fromStdString(monster->damageImmunities));
  m_ui->lneDamageVulnerability->setText(QString::fromStdString(monster->damageVulnerabilities));
  m_ui->lneConditionImmunity->setText(QString::fromStdString(monster->conditionImmunities));

  //stats
  for (const auto& s : monster->stats)
  {
    auto& widgets = m_statWidgets[s.first];
    widgets.first->setValue(s.second);
  }

  //saving throws
  for (const auto& st : monster->savingThrows)
  {
    auto& modInfo = m_savingThrowWidgets[st.first];

    modInfo.autoCalc->setChecked(st.second.autoCalculate);
    modInfo.proficient->setChecked(st.second.proficient);
    modInfo.modifier->setValue(st.second.modifier);
  }

  //skills
  for (const auto& s : monster->skills)
  {
    auto& modInfo = m_skillWidgets[s.first];

    modInfo.autoCalc->setChecked(s.second.autoCalculate);
    modInfo.proficient->setChecked(s.second.proficient);
    modInfo.modifier->setValue(s.second.modifier);
  } //end  for (const auto& s : monster->skills)
}
std::shared_ptr<HM::HomebrewItem> MonsterEditor::getDefault()
{
  return std::make_shared<HM::Monster>();
}

//constructors
MonsterEditor::MonsterEditor(std::shared_ptr<cheeto::OutputManager> log, QWidget* parent) :
  MonsterEditor(log, "", parent)
{
}
MonsterEditor::MonsterEditor(std::shared_ptr<cheeto::OutputManager> log, std::string filePath,
                             QWidget* parent) :
  EditorWidget(log, parent),
  m_ui(std::make_unique<Ui_MonsterEditor>())
{
  m_lastSavedVersion = std::make_shared<HM::Monster>();

  m_ui->setupUi(this);

  //populate size combobox
  for (const auto& size : HM::Monster::SIZE_STR_MAP)
  {
    m_ui->cmbSize->addItem(QString::fromStdString(size.second));
  } //end  for (const auto& size : HM::Monster::SIZE_STR_MAP)

  //populate type combobox
  for (const auto& type : HM::Monster::TYPE_STR_MAP)
  {
    m_ui->cmbType->addItem(QString::fromStdString(type.second));
  } //end  for (const auto& size : HM::Monster::SIZE_STR_MAP)

  //populate alignment combobox
  for (const auto& alignment : HM::Monster::ALIGNMENT_STR_MAP)
  {
    m_ui->cmbAlignment->addItem(QString::fromStdString(alignment.second));
  } //end  for (const auto& size : HM::Monster::SIZE_STR_MAP)

  //build up map of stat widgets
  m_statWidgets[HM::Monster::Stat::CHARISMA] = {m_ui->spnChaScore, m_ui->lblChaMod};
  m_statWidgets[HM::Monster::Stat::CONSTITUTION] = {m_ui->spnConScore, m_ui->lblConMod};
  m_statWidgets[HM::Monster::Stat::DEXTERITY] = {m_ui->spnDexScore, m_ui->lblDexMod};
  m_statWidgets[HM::Monster::Stat::INTELLIGENCE] = {m_ui->spnIntScore, m_ui->lblIntMod};
  m_statWidgets[HM::Monster::Stat::STRENGTH] = {m_ui->spnStrScore, m_ui->lblStrMod};
  m_statWidgets[HM::Monster::Stat::WISDOM] = {m_ui->spnWisScore, m_ui->lblWisMod};

  //build up map of saving throw widgets
  m_savingThrowWidgets[HM::Monster::Stat::CHARISMA] = {m_ui->chkChaAuto,
                                                       m_ui->chkChaProf,
                                                       m_ui->spnChaSaveMod};
  m_savingThrowWidgets[HM::Monster::Stat::CONSTITUTION] = {m_ui->chkConAuto,
                                                           m_ui->chkConProf,
                                                           m_ui->spnConSaveMod};
  m_savingThrowWidgets[HM::Monster::Stat::DEXTERITY] = {m_ui->chkDexAuto,
                                                        m_ui->chkDexProf,
                                                        m_ui->spnDexSaveMod};
  m_savingThrowWidgets[HM::Monster::Stat::INTELLIGENCE] = {m_ui->chkIntAuto,
                                                           m_ui->chkIntProf,
                                                           m_ui->spnIntSaveMod};
  m_savingThrowWidgets[HM::Monster::Stat::STRENGTH] = {m_ui->chkStrAuto,
                                                       m_ui->chkStrProf,
                                                       m_ui->spnStrSaveMod};
  m_savingThrowWidgets[HM::Monster::Stat::WISDOM] = {m_ui->chkWisAuto,
                                                     m_ui->chkWisProf,
                                                     m_ui->spnWisSaveMod};

  //build up map of skill widgets
  m_skillWidgets[HM::Monster::Skill::ACROBATICS] = {m_ui->chkAcrobaticsAuto,
                                                    m_ui->chkAcrobaticsProf,
                                                    m_ui->spnAcrobaticsMod};
  m_skillWidgets[HM::Monster::Skill::ANIMAL_HANDLING] = {m_ui->chkAnimalHandlingAuto,
                                                         m_ui->chkAnimalHandlingProf,
                                                         m_ui->spnAnimalHandlingMod};
  m_skillWidgets[HM::Monster::Skill::ARCANA] = {m_ui->chkArcanaAuto,
                                                m_ui->chkArcanaProf,
                                                m_ui->spnArcanaMod};
  m_skillWidgets[HM::Monster::Skill::ATHLETICS] = {m_ui->chkAthleticsAuto,
                                                   m_ui->chkAthleticsProf,
                                                   m_ui->spnAthleticsMod};
  m_skillWidgets[HM::Monster::Skill::DECEPTION] = {m_ui->chkDeceptionAuto,
                                                   m_ui->chkDeceptionProf,
                                                   m_ui->spnDeceptionMod};
  m_skillWidgets[HM::Monster::Skill::HISTORY] = {m_ui->chkHistoryAuto,
                                                 m_ui->chkHistoryProf,
                                                 m_ui->spnHistoryMod};
  m_skillWidgets[HM::Monster::Skill::INSIGHT] = {m_ui->chkInsightAuto,
                                                 m_ui->chkInsightProf,
                                                 m_ui->spnInsightMod};
  m_skillWidgets[HM::Monster::Skill::INTIMIDATION] = {m_ui->chkIntimidationAuto,
                                                      m_ui->chkIntimidationProf,
                                                      m_ui->spnIntimidationMod};
  m_skillWidgets[HM::Monster::Skill::INVESTIGATION] = {m_ui->chkInvestigationAuto,
                                                       m_ui->chkInvestigationProf,
                                                       m_ui->spnInvestigationMod};
  m_skillWidgets[HM::Monster::Skill::MEDICINE] = {m_ui->chkMedicineAuto,
                                                  m_ui->chkMedicineProf,
                                                  m_ui->spnMedicineMod};
  m_skillWidgets[HM::Monster::Skill::NATURE] = {m_ui->chkNatureAuto,
                                                m_ui->chkNatureProf,
                                                m_ui->spnNatureMod};
  m_skillWidgets[HM::Monster::Skill::PERCEPTION] = {m_ui->chkPerceptionAuto,
                                                    m_ui->chkPerceptionProf,
                                                    m_ui->spnPerceptionMod};
  m_skillWidgets[HM::Monster::Skill::PERFORMANCE] = {m_ui->chkPerformanceAuto,
                                                     m_ui->chkPerformanceProf,
                                                     m_ui->spnPerformanceMod};
  m_skillWidgets[HM::Monster::Skill::PERSUASION] = {m_ui->chkPersuasionAuto,
                                                    m_ui->chkPersuasionProf,
                                                    m_ui->spnPersuasionMod};
  m_skillWidgets[HM::Monster::Skill::RELIGION] = {m_ui->chkReligionAuto,
                                                  m_ui->chkReligionProf,
                                                  m_ui->spnReligionMod};
  m_skillWidgets[HM::Monster::Skill::SLEIGHT_OF_HAND] = {m_ui->chkSleightOfHandAuto,
                                                         m_ui->chkSleightOfHandProf,
                                                         m_ui->spnSleightOfHandMod};
  m_skillWidgets[HM::Monster::Skill::STEALTH] = {m_ui->chkStealthAuto,
                                                 m_ui->chkStealthProf,
                                                 m_ui->spnStealthMod};
  m_skillWidgets[HM::Monster::Skill::SURVIVAL] = {m_ui->chkSurvivalAuto,
                                                  m_ui->chkSurvivalProf,
                                                  m_ui->spnSurvivalMod};

  makeConnections();

  //set window state to default monster
  clearGui();
}

//EditorWidget overrides
bool MonsterEditor::unsavedChanges()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: MonsterEditor: unsavedChanges()\n";

  auto lhs = std::dynamic_pointer_cast<HM::Monster>(m_lastSavedVersion);
  auto rhs = std::dynamic_pointer_cast<HM::Monster>(fromGui());

  return *lhs == *rhs;
}

//public slots
void MonsterEditor::statScoreUpdated(HM::Monster::Stat stat, int newScore)
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: MonsterEditor: statScoreUpdated()\n";
  auto& widgets = m_statWidgets[stat];

  //update stat modifier
  widgets.second->setText(QString::number(this->calcModifier(newScore, false)));

  //update saving throw value when changed
  updateModInfo(stat, m_savingThrowWidgets[stat]);

  //update skill modifier when changed
  for (const auto& skill : HM::Monster::SKILL_STAT_MAP)
  {
    if (skill.second == stat)
    {
      updateModInfo(stat, m_skillWidgets[skill.first]);
    } //end  if (skill.second == stat)
  } //end  for (const auto& skill : HM::Monster::SKILL_STAT_MAP)
}
void MonsterEditor::savingThrowAutoUpdated(HM::Monster::Stat stat, bool autoUpdate)
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: MonsterEditor: savingThrowAutoUpdated()\n";

  //get widgets involved
  auto& widgets = m_savingThrowWidgets[stat];

  //update widget enabled status
  widgets.proficient->setEnabled(autoUpdate);
  widgets.modifier->setEnabled(!autoUpdate);

  //update saving throw info if necessary
  updateModInfo(stat, widgets);
}
void MonsterEditor::savingThrowProfUpdated(HM::Monster::Stat stat)
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: MonsterEditor: savingThrowProfUpdated()\n";

  updateModInfo(stat, m_savingThrowWidgets[stat]);
}
void MonsterEditor::skillAutoUpdated(HM::Monster::Skill skill, bool autoUpdate)
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: MonsterEditor: skillAutoUpdated()\n";

  //get widgets involved
  auto& widgets = m_skillWidgets[skill];

  //update widget enabled status
  widgets.proficient->setEnabled(autoUpdate);
  widgets.modifier->setEnabled(!autoUpdate);

  //update saving throw info if necessary
  updateModInfo(HM::Monster::SKILL_STAT_MAP.at(skill), widgets);
}
void MonsterEditor::skillProfUpdated(HM::Monster::Skill skill)
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: MonsterEditor: skillProfUpdated()\n";

  updateModInfo(HM::Monster::SKILL_STAT_MAP.at(skill), m_skillWidgets[skill]);
}
void MonsterEditor::profBonusUpdated()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: MonsterEditor: profBonusUpdated()\n";

  for (auto& savingThrow : m_savingThrowWidgets)
  {
    updateModInfo(savingThrow.first, savingThrow.second);
  } //end  for (const auto& savingThrow : m_savingThrowWidgets)

  for (auto& skill : m_skillWidgets)
  {
    updateModInfo(HM::Monster::SKILL_STAT_MAP.at(skill.first), skill.second);
  } //end  for (const auto& skill : m_skillWidgets)
};
