#include <QApplication>
#include <QCommandLineParser>

#include "SelectionScreen.h"

using CheesySoftware::HomebrewManager::AppOptions;
using CheesySoftware::HomebrewManager::Verbosity;
using CheesySoftware::HomebrewManager::Editor;

const QCommandLineOption verbosityOption = {{"v", "verbosity"}, "Verbosity of the info displayed", "verbosityLevel", "3"};
const QCommandLineOption logPathOption = {{"l", "log-path"}, "path to put the log file", "logPath", "./"};
const QCommandLineOption editorOption = {{"e", "editor"}, "Editor to open to instead of selection screen.\n Valid options: class, encounter, feat, item, monster, npc, race, spell", "editor", ""};

bool parseCmd(QCommandLineParser& parser, AppOptions& options);

int main(int argc, char** argv)
{
  QApplication app(argc, argv);

  //setup application information
  QCoreApplication::setApplicationName("Homebrew Manager");
  QCoreApplication::setApplicationVersion("0.0.0");
  QCoreApplication::setOrganizationName("Cheesy Software");

  //parse cmd args
  QCommandLineParser parser;
  AppOptions options;
  if (!parseCmd(parser, options))
  {
    parser.showHelp(1);
  } //end  if (!parseCmd(parser, options))

  //create and show main window
  SelectionScreen win(options);
  if (options.editor == Editor::NONE)
  {
    win.show();
  }

  return app.exec();
}

bool parseCmd(QCommandLineParser& parser, AppOptions& options)
{
  parser.addHelpOption();
  parser.setApplicationDescription("Homebrew manager for D&D. Used to create and edit all homebrew content");

  parser.addOptions({verbosityOption, logPathOption, editorOption});

  parser.process(QCoreApplication::arguments());

  bool conversionSuccess = false;

  //verbosity
  int verbosity = parser.value(verbosityOption).toInt(&conversionSuccess);
  if (!conversionSuccess)
  {
    return false;
  }
  verbosity = std::max((int)Verbosity::NO_INFO, verbosity);
  verbosity = std::min((int)Verbosity::ALL_INFO, verbosity);
  options.verbosity = (Verbosity)verbosity;

  //log file
  options.logPath = parser.value(logPathOption).toStdString();
  if (options.logPath.empty())
  {
    options.logPath = "./homebrew_manager.log";
  }
  else
  {
    options.logPath += "/homebrew_manager.log";
  }

  //editor to open
  if (parser.isSet(editorOption))
  {
    options.editor = AppOptions::editorFromStr(parser.value(editorOption).toStdString());
  }

  return true;
}

