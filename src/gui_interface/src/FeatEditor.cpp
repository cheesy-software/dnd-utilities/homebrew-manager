#include "FeatEditor.h"

#include "ApplicationOptions.h"
#include "OutputManager.h"
#include "Feat.h"

//private functions
void FeatEditor::makeConnections()
{
  (*m_log) << ((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: FeatEditor: makeConnections()\n";
}

//EditorWidget overrides
std::shared_ptr<HM::HomebrewItem> FeatEditor::fromGui()
{
  (*m_log) << ((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: FeatEditor: fromGui()\n";
  return std::make_shared<HM::Feat>();
}
void FeatEditor::toGui(const std::shared_ptr<HM::HomebrewItem>&)
{
  (*m_log) << ((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: FeatEditor: toGui()\n";
}
std::shared_ptr<HM::HomebrewItem> FeatEditor::getDefault()
{
  (*m_log) << ((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: FeatEditor: getDefault()\n";

  return std::make_shared<HM::Feat>();
}

//constructors
FeatEditor::FeatEditor(std::shared_ptr<cheeto::OutputManager> log, QWidget* parent) :
  FeatEditor(log, "", parent)
{
}
FeatEditor::FeatEditor(std::shared_ptr<cheeto::OutputManager> log, std::string filePath,
           QWidget* parent) :
  EditorWidget(log, parent),
  m_ui(std::make_unique<Ui_FeatEditor>())
{
  (*m_log) << ((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: FeatEditor: constructor()\n";

  m_lastSavedVersion = std::make_shared<HM::Feat>();

  m_ui->setupUi(this);

  makeConnections();
}

//EditorWidget overrides
bool FeatEditor::unsavedChanges()
{
  (*m_log) << ((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: FeatEditor: unsavedChanges()\n";

  auto lhs = std::dynamic_pointer_cast<HM::Feat>(m_lastSavedVersion);
  auto rhs = std::dynamic_pointer_cast<HM::Feat>(fromGui());

  return *lhs == *rhs;
}

