#include "HomebrewWindow.h"

#include <QMessageBox>
#include <QFileDialog>

struct Ui_HomebrewWindow
{
  QAction *actNew;
  QAction *actOpen;
  QAction *actSave;
  QAction *actQuit;
  QWidget *centralwidget;
  QVBoxLayout *verticalLayout;
  EditorWidget *wgtEditor;
  QPushButton *btnSave;
  QMenuBar *menubar;
  QMenu *mnuFile;

  void setupUi(HomebrewWindow* parent, EditorWidget* editorWidget)
  {
    parent->setWindowTitle(editorWidget->windowTitle());
    parent->resize(800, 600);
    actNew = new QAction("New", parent);
    actOpen = new QAction("Open", parent);
    actSave = new QAction("Save", parent);
    actQuit = new QAction("Quit", parent);
    centralwidget = new QWidget(parent);
    verticalLayout = new QVBoxLayout(centralwidget);
    wgtEditor = editorWidget;

    verticalLayout->addWidget(wgtEditor);

    btnSave = new QPushButton("Save", centralwidget);

    verticalLayout->addWidget(btnSave);

    parent->setCentralWidget(centralwidget);
    menubar = new QMenuBar(parent);
    menubar->setGeometry(QRect(0, 0, 800, 21));
    mnuFile = new QMenu("File", menubar);
    parent->setMenuBar(menubar);

    menubar->addAction(mnuFile->menuAction());
    mnuFile->addAction(actNew);
    mnuFile->addAction(actOpen);
    mnuFile->addAction(actSave);
    mnuFile->addSeparator();
    mnuFile->addAction(actQuit);
  }
};

//private functions
void HomebrewWindow::makeConnections()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: HomebrewWindow: makeConnections()\n";

  connect(m_ui->btnSave, &QPushButton::clicked, this, &HomebrewWindow::btnSaveHandler);

  connect(m_ui->actNew, &QAction::triggered, this, &HomebrewWindow::actNewHandler);
  connect(m_ui->actOpen, &QAction::triggered, this, &HomebrewWindow::actOpenHandler);
  connect(m_ui->actSave, &QAction::triggered, this, &HomebrewWindow::actSaveHandler);
  connect(m_ui->actQuit, &QAction::triggered, this, &HomebrewWindow::actQuitHandler);
}
bool HomebrewWindow::confirmWorkLoss()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: HomebrewWindow: confirmWorkLoss()\n";

  //if there are no unsaved changes, automatically return true
  if (m_ui->wgtEditor->unsavedChanges())
  {
    (*m_log)((int)HM::Verbosity::DEBUG_INFO) << "DEBUG: HomebrewWindow: Last saved version is equivalent to current GUI. skipping confirmation ask\n";
    return true;
  } //end  if (wgtEditor->unsavedChanges())

  QMessageBox box;
  box.setText("You have unsaved changes");
  box.setInformativeText("This action will cause work to be lost. Do you wish to continue?");
  box.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
  box.setDefaultButton(QMessageBox::No);

  auto selection = box.exec();
  return selection == QMessageBox::Yes;
}
void HomebrewWindow::save()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: HomebrewWindow: save()\n";

  //get filename to save to
  auto fileName = QFileDialog::getSaveFileName(this, "Save", "", "JSON Files (*.json)");
  if (fileName.isEmpty())
  {
    (*m_log)((int)HM::Verbosity::VERBOSE_INFO) << "INFO: HomebrewWindow: saving canceled\n";
    return;
  }
  (*m_log)((int)HM::Verbosity::VERBOSE_INFO) << "INFO: HomebrewWindow: Saving file to: "
                                             << fileName.toStdString() << "\n";

  m_ui->wgtEditor->saveFile(fileName);
}
void HomebrewWindow::load()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: HomebrewWindow: load()\n";

  if (!confirmWorkLoss())
  {
    (*m_log)((int)HM::Verbosity::VERBOSE_INFO) << "INFO: HomebrewWindow: user doesn't wish to overrite work, skipping file loading\n";
    return;
  } //end  if (!confirmWorkLoss())

  //get filename of where to load file from
  auto fileName = QFileDialog::getOpenFileName(this, "Open", "", "JSON Files (*.json);;All Files (*)");
  if (fileName.isEmpty())
  {
    (*m_log)((int)HM::Verbosity::VERBOSE_INFO) << "INFO: HomebrewWindow: file loading canceled\n";
    return;
  } //end  if (fileName.isEmpty())
  (*m_log)((int)HM::Verbosity::VERBOSE_INFO) << "INFO: HomebrewWindow: Opening file: "
                                             << fileName.toStdString() << "\n";

  //load file
  m_ui->wgtEditor->loadFile(fileName);
}
void HomebrewWindow::clear()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: HomebrewWindow: clear()\n";

  //unsaved changes check
  if (!confirmWorkLoss())
  {
    (*m_log)((int)HM::Verbosity::VERBOSE_INFO) << "INFO: HomebrewWindow: user decided to not clear GUI\n";
    return;
  } //end  if (!confirmWorkLoss())

  m_ui->wgtEditor->clearGui();
}

//event handlers
void HomebrewWindow::closeEvent(QCloseEvent* e)
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: HomebrewWindow: closeEvent()\n";

  if (!confirmWorkLoss())
  {
    (*m_log)((int)HM::Verbosity::VERBOSE_INFO) << "INFO: HomebrewWindow: user canceled close\n";
    e->ignore();
    return;
  } //end  if (!confirmWorkLoss())

  emit closing();
  QMainWindow::closeEvent(e);
}

//constructors
HomebrewWindow::HomebrewWindow(EditorWidget* editorWidget, std::shared_ptr<cheeto::OutputManager> log,
                               QWidget* parent) :
  m_ui(std::make_shared<Ui_HomebrewWindow>()),
  m_log(log)
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: HomebrewWindow: constructor\n";
  m_ui->setupUi(this, editorWidget);
  makeConnections();
}

//public slots
void HomebrewWindow::btnSaveHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: HomebrewWindow: btnSaveHandler()\n";

  save();
}
void HomebrewWindow::actNewHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: HomebrewWindow: actNewHandler()\n";

  clear();
}
void HomebrewWindow::actOpenHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: HomebrewWindow: actOpenHandler()\n";

  load();
}
void HomebrewWindow::actSaveHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: HomebrewWindow: actSaveHandler()\n";

  save();
}
void HomebrewWindow::actQuitHandler()
{
  (*m_log)((int)HM::Verbosity::FUNCTION_NAME) << "FNAME: HomebrewWindow: actQuitHandler()\n";

  close();
}

