#ifndef CHEESY_SOFTWARE_HOMEBREW_MANAGER_HOMEBREW_ITEM_H
#define CHEESY_SOFTWARE_HOMEBREW_MANAGER_HOMEBREW_ITEM_H

#include <string>

namespace CheesySoftware
{
  namespace HomebrewManager
  {
    struct HomebrewItem
    {
      virtual bool loadFromFile(const std::string& filePath) = 0;
      virtual bool saveToFile(const std::string& filePath) = 0;
    };
  }
}

#endif
