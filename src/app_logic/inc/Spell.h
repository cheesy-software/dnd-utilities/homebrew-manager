#ifndef CHEESY_SOFTWARE_HOMEBREW_MANAGER_SPELL_H
#define CHEESY_SOFTWARE_HOMEBREW_MANAGER_SPELL_H

//std lib includes
#include <set>
#include <map>
#include <iostream>

#include "HomebrewItem.h"

namespace CheesySoftware
{
  namespace HomebrewManager
  {
    struct Spell : public HomebrewItem
    {
      //file version
      struct Version
      {
        int major;
        int minor;
        Version(int major, int minor)
        {
          this->major = major;
          this->minor = minor;
        }
      };

      enum class School
      {
        ABJURATION,
        CONJURATION,
        DIVINATION,
        ENCHANTMENT,
        EVOCATION,
        ILLUSION,
        NECROMANCY,
        TRANSMUTATION,
      };
      static const std::map<School, std::string> SCHOOL_STR_MAP;
      enum class Class
      {
        BARD,
        CLERIC,
        DRUID,
        PALADIN,
        RANGER,
        SORCERER,
        WARLOCK,
        WIZARD
      };
      static const std::map<Class, std::string> CLASS_STR_MAP;
      struct Components
      {
        bool verbal = false;
        bool somatic = false;
        bool material = false;
        std::string materialComponents = "";
      };

      static const Version VERSION;

      std::string name = "";
      School school = School::ABJURATION;
      int level = 0;
      std::string castingTime = "";
      std::string range = "";
      std::string duration = "";
      Components components = Components();
      bool concentration = false;
      bool ritual = false;
      std::set<Class> classes;
      std::string effect = "";
      std::string upcastEffect = "";

      //functions
      virtual bool loadFromFile(const std::string& filePath) override;
      virtual bool saveToFile(const std::string& filePath) override;
    };

    //comparison operators
    bool operator==(const Spell::Components& lhs, const Spell::Components& rhs);
    bool operator!=(const Spell::Components& lhs, const Spell::Components& rhs);
    bool operator==(const Spell& lhs, const Spell& rhs);
    bool operator!=(const Spell& lhs, const Spell& rhs);

    //ostream operators
    std::ostream& operator<<(std::ostream& out, const Spell& s);
    std::ostream& operator<<(std::ostream& out, const Spell::Components& c);
  }
}


#endif
