#ifndef CHEESY_SOFTWARE_HOMEBREW_MANAGER_MONSTER_H
#define CHEESY_SOFTWARE_HOMEBREW_MANAGER_MONSTER_H

#include <map>
#include <set>
#include <iostream>

#include "HomebrewItem.h"

namespace CheesySoftware
{
  namespace HomebrewManager
  {
    struct Monster : public HomebrewItem
    {
      //file version
      struct Version
      {
        int major;
        int minor;
        Version(int major, int minor)
        {
          this->major = major;
          this->minor = minor;
        }
      };

      enum class Size
      {
        TINY,
        SMALL,
        MEDIUM,
        LARGE,
        HUGE,
        GARGANTUAN,
        VARIES
      };
      static const std::map<Size, std::string> SIZE_STR_MAP;
      enum class Type
      {
        ABERRATION,
        BEAST,
        CELESTIAL,
        CONSTRUCT,
        DRAGON,
        ELEMENTAL,
        FEY,
        FIEND,
        GIANT,
        HUMANOID,
        MONSTROSITY,
        OOZE,
        PLANT,
        UNDEAD
      };
      static const std::map<Type, std::string> TYPE_STR_MAP;
      enum class Alignment
      {
        LAWFUL_GOOD,
        NEUTRAL_GOOD,
        CHAOTIC_GOOD,
        LAWFUL_NEUTRAL,
        TRUE_NEUTRAL,
        CHAOTIC_NEUTRAL,
        LAWFUL_EVIL,
        NEUTRAL_EVIL,
        CHAOTIC_EVIL
      };
      static const std::map<Alignment, std::string> ALIGNMENT_STR_MAP;
      enum class Stat
      {
        STRENGTH,
        DEXTERITY,
        CONSTITUTION,
        INTELLIGENCE,
        WISDOM,
        CHARISMA
      };
      static const std::map<Stat, std::string> STAT_STR_MAP;
      enum class Skill
      {
        ACROBATICS,
        ANIMAL_HANDLING,
        ARCANA,
        ATHLETICS,
        DECEPTION,
        HISTORY,
        INSIGHT,
        INTIMIDATION,
        INVESTIGATION,
        MEDICINE,
        NATURE,
        PERCEPTION,
        PERFORMANCE,
        PERSUASION,
        RELIGION,
        SLEIGHT_OF_HAND,
        STEALTH,
        SURVIVAL
      };
      static const std::map<Skill, std::string> SKILL_STR_MAP;
      static const std::map<Skill, Stat> SKILL_STAT_MAP;

      struct ModifierInfo
      {
        bool autoCalculate;
        bool proficient;
        std::int64_t modifier; //only valid if autoCalculate is false
      };

      static const Version VERSION;

      std::string name;
      Size size = Size::MEDIUM;
      Type type = Type::HUMANOID;
      Alignment alignment;
      std::int64_t ac;
      std::string acReason;
      std::int64_t hp;
      std::string hpDice;
      std::string speed;
      std::string senses;
      std::string languages;
      std::string challengeRating;
      std::int64_t xp;
      std::map<Stat, std::int64_t> stats = {
        {Stat::CHARISMA, 10},
        {Stat::CONSTITUTION, 10},
        {Stat::DEXTERITY, 10},
        {Stat::INTELLIGENCE, 10},
        {Stat::STRENGTH, 10},
        {Stat::WISDOM, 10},
      };
      std::map<Stat, ModifierInfo> savingThrows = {
        {Stat::STRENGTH, {true, false, 0}},
        {Stat::DEXTERITY, {true, false, 0}},
        {Stat::CONSTITUTION, {true, false, 0}},
        {Stat::INTELLIGENCE, {true, false, 0}},
        {Stat::WISDOM, {true, false, 0}},
        {Stat::CHARISMA, {true, false, 0}}
      };
      std::map<Skill, ModifierInfo> skills = {
        {Skill::ACROBATICS, {true, false, 0}},
        {Skill::ANIMAL_HANDLING, {true, false, 0}},
        {Skill::ARCANA, {true, false, 0}},
        {Skill::ATHLETICS, {true, false, 0}},
        {Skill::DECEPTION, {true, false, 0}},
        {Skill::HISTORY, {true, false, 0}},
        {Skill::INSIGHT, {true, false, 0}},
        {Skill::INTIMIDATION, {true, false, 0}},
        {Skill::INVESTIGATION, {true, false, 0}},
        {Skill::MEDICINE, {true, false, 0}},
        {Skill::NATURE, {true, false, 0}},
        {Skill::PERCEPTION, {true, false, 0}},
        {Skill::PERFORMANCE, {true, false, 0}},
        {Skill::PERSUASION, {true, false, 0}},
        {Skill::RELIGION, {true, false, 0}},
        {Skill::SLEIGHT_OF_HAND, {true, false, 0}},
        {Skill::STEALTH, {true, false, 0}},
        {Skill::SURVIVAL, {true, false, 0}}
      };
      std::string abilities;
      std::string actions;
      std::string info;
      std::int64_t proficiencyBonus = 2;
      std::string damageResistances;
      std::string damageImmunities;
      std::string damageVulnerabilities;
      std::string conditionImmunities;

      bool loadFromFile(const std::string& filePath) override;
      bool saveToFile(const std::string& filePath) override;
    };

    //comparison operators
    bool operator==(const Monster::ModifierInfo& lhs, const Monster::ModifierInfo& rhs);
    bool operator!=(const Monster::ModifierInfo& lhs, const Monster::ModifierInfo& rhs);
    bool operator==(const Monster& lhs, const Monster& rhs);
    bool operator!=(const Monster& lhs, const Monster& rhs);

    //ostream operators
    std::ostream& operator<<(std::ostream& out, const Monster::ModifierInfo& m);
    std::ostream& operator<<(std::ostream& out, const Monster& m);
  }
}

#endif
