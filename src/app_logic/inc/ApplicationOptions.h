#ifndef CHEESY_SOFTWARE_HOMEBREW_MANAGER_APPLICATION_OPTIONS_H
#define CHEESY_SOFTWARE_HOMEBREW_MANAGER_APPLICATION_OPTIONS_H

#include <ostream>
#include <string>

namespace CheesySoftware
{
  namespace HomebrewManager
  {
    enum class Verbosity
    {
      NO_INFO,       //0
      ERROR,         //1
      WARNING,       //2
      INFO,          //3
      VERBOSE_INFO,  //4
      DEBUG_INFO,    //5
      FUNCTION_NAME, //6
      ALL_INFO       //7
    };

    enum class Editor
    {
      CLASS,
      ENCOUNTER,
      FEAT,
      ITEM,
      MONSTER,
      NPC,
      RACE,
      SPELL,
      NONE
    };

    struct AppOptions
    {
      Verbosity verbosity = Verbosity::INFO;
      std::string logPath = "./";
      Editor editor = Editor::NONE;

      static Editor editorFromStr(const std::string& str);
    };
  }
}

std::ostream& operator<<(std::ostream& out, const CheesySoftware::HomebrewManager::Verbosity& toPrint);

#endif
