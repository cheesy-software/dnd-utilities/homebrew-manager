#ifndef CHEESY_SOFTWARE_HOMEBREW_MANAGER_FEAT_H
#define CHEESY_SOFTWARE_HOMEBREW_MANAGER_FEAT_H

//std lib includes
#include <string>
#include <vector>

#include "HomebrewItem.h"

namespace CheesySoftware
{
  namespace HomebrewManager
  {
    struct Feat : public HomebrewItem
    {
      std::string name = "";
      std::vector<std::string> prerequisites;
      std::string flavorText;
      std::vector<std::string> abilities;

      //functions
      virtual bool loadFromFile(const std::string& filePath) override;
      virtual bool saveToFile(const std::string& filePath) override;
    };

    //comparison operators
    bool operator==(const Feat& lhs, const Feat& rhs);
    bool operator!=(const Feat& lhs, const Feat& rhs);

    //ostream operators
    std::ostream& operator<<(std::ostream& out, const Feat& f);
  }
}


#endif
