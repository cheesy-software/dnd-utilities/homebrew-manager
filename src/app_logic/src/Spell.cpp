#include "Spell.h"

//std lib includes
#include <fstream>

//third party includes
#include "json.hpp"

namespace CheesySoftware
{
  namespace HomebrewManager
  {
    const Spell::Version Spell::VERSION = Spell::Version(1, 0);
    const std::map<Spell::School, std::string> Spell::SCHOOL_STR_MAP = {
      {School::ABJURATION,    "Abjuration"},
      {School::CONJURATION,   "Conjuration"},
      {School::DIVINATION,    "Divination"},
      {School::ENCHANTMENT,   "Enchantment"},
      {School::EVOCATION,     "Evocation"},
      {School::ILLUSION,      "Illusion"},
      {School::NECROMANCY,    "Necromancy"},
      {School::TRANSMUTATION, "Transmutation"}
    };
    const std::map<Spell::Class, std::string> Spell::CLASS_STR_MAP = {
      {Class::BARD,     "Bard"},
      {Class::CLERIC,   "Cleric"},
      {Class::DRUID,    "Druid"},
      {Class::PALADIN,  "Paladin"},
      {Class::RANGER,   "Ranger"},
      {Class::SORCERER, "Sorcerer"},
      {Class::WARLOCK,  "Warlock"},
      {Class::WIZARD,   "Wizard"}
    };

    void to_json(nlohmann::json& j, const Spell::Components& c)
    {
      j = {
        {"verbal", c.verbal},
        {"somatic", c.somatic},
        {"material", c.material},
        {"materialComponents", c.materialComponents}
      };
    }
    void from_json(const nlohmann::json& j, Spell::Components& c)
    {
      j.at("verbal").get_to(c.verbal);
      j.at("somatic").get_to(c.somatic);
      j.at("material").get_to(c.material);
      j.at("materialComponents").get_to(c.materialComponents);
    }

    void to_json(nlohmann::json& j, const Spell::Version& v)
    {
      j = {
        {"major", v.major},
        {"minor", v.minor}
      };
    }
    void from_json(const nlohmann::json& j, Spell::Version& v)
    {
      j.at("major").get_to(v.major);
      j.at("minor").get_to(v.minor);
    }

    void to_json(nlohmann::json& j, const Spell& s)
    {
      j = {
        {"version", s.VERSION},
        {"name", s.name},
        {"school", s.school},
        {"level", s.level},
        {"castingTime", s.castingTime},
        {"range", s.range},
        {"duration", s.duration},
        {"components", s.components},
        {"concentration", s.concentration},
        {"ritual", s.ritual},
        {"classes", s.classes},
        {"effect", s.effect},
        {"upcastEffect", s.upcastEffect}
      };
    }
    void from_json(const nlohmann::json& j, Spell& s)
    {
      j.at("name").get_to(s.name);
      j.at("school").get_to(s.school);
      j.at("level").get_to(s.level);
      j.at("castingTime").get_to(s.castingTime);
      j.at("range").get_to(s.range);
      j.at("duration").get_to(s.duration);
      j.at("components").get_to(s.components);
      j.at("concentration").get_to(s.concentration);
      j.at("ritual").get_to(s.ritual);
      j.at("classes").get_to(s.classes);
      j.at("effect").get_to(s.effect);
      j.at("upcastEffect").get_to(s.upcastEffect);
    }

    bool Spell::loadFromFile(const std::string& filePath)
    {
      nlohmann::json j;
      std::ifstream f(filePath);

      try
      {
        f >> j;
        j.at("spell").get_to(*this);
        //TODO version handling
      } //end  try
      catch (std::exception e)
      {
        //TODO actually catch real exception and use logger to print
        std::cerr << "Error reading spell json file " << filePath << std::endl;
        std::cerr << e.what() << std::endl;
        return false;
      } //end  catch (std::exception e)

      return true;
    }
    bool Spell::saveToFile(const std::string& filePath)
    {
      std::ofstream f(filePath);
      nlohmann::json j;
      j["spell"] = *this;

      try
      {
        f << j.dump(4);
      }
      catch(std::exception e)
      {
        //TODO actuall catch real exception and use logger to print
        std::cerr << "Error writing spell json file " << filePath << std::endl;
        std::cerr << e.what() << std::endl;
        return false;
      }

      return true;
    }

    //comparison operators
    bool operator==(const Spell::Components& lhs, const Spell::Components& rhs)
    {
      if (lhs.verbal == rhs.verbal &&
          lhs.somatic == rhs.somatic &&
          lhs.material == rhs.material)
      {
        if (lhs.material)
        {
          return lhs.materialComponents == rhs.materialComponents;
        } //end  if (lhs.material)
        return true;
      } //end  if (lhs.verbal == rhs.material && lhs.somatic == rhs.somatic && lhs.material == rhs.material)
      return false;
    }
    bool operator!=(const Spell::Components& lhs, const Spell::Components& rhs)
    {
      return !(lhs == rhs);
    }
    bool operator==(const Spell& lhs, const Spell& rhs)
    {
      return lhs.name == rhs.name &&
             lhs.school == rhs.school &&
             lhs.level == rhs.level &&
             lhs.castingTime == rhs.castingTime &&
             lhs.range == rhs.range &&
             lhs.duration == rhs.duration &&
             lhs.components == rhs.components &&
             lhs.concentration == rhs.concentration &&
             lhs.ritual == rhs.ritual &&
             lhs.classes == rhs.classes &&
             lhs.effect == rhs.effect &&
             lhs.upcastEffect == rhs.upcastEffect;
    }
    bool operator!=(const Spell& lhs, const Spell& rhs)
    {
      return !(lhs == rhs);
    }

    //ostream operators
    std::ostream& operator<<(std::ostream& out, const Spell& s)
    {
      nlohmann::json j;
      j["spell"] = s;
      out << j.dump(4);
      return out;
    }
    std::ostream& operator<<(std::ostream& out, const Spell::Components& c)
    {
      nlohmann::json j;
      j["components"] = c;
      out << j.dump(4);
      return out;
    }
  }
}
