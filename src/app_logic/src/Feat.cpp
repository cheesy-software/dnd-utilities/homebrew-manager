#include "Feat.h"

//std lib includes
#include <fstream>
#include <iostream> // temporary until output manager is added

//third party includes
#include "json.hpp"

namespace CheesySoftware
{
  namespace HomebrewManager
  {
    void to_json(nlohmann::json& j, const Feat& f)
    {
      j = {
        {"name", f.name},
        {"prerequisites", f.prerequisites},
        {"flavorText", f.flavorText},
        {"abilities", f.abilities},
      };
    }
    void from_json(const nlohmann::json& j, Feat& f)
    {
        j.at("name").get_to(f.name);
        j.at("prerequisites").get_to(f.prerequisites);
        j.at("flavorText").get_to(f.flavorText);
        j.at("abilities").get_to(f.abilities);
    }

    bool Feat::loadFromFile(const std::string& filePath)
    {
      nlohmann::json j;
      std::ifstream f(filePath);

      try
      {
        f >> j;
        j.at("feat").get_to(*this);
      } //end  try
      catch (std::exception e)
      {
        //TODO actually catch real exception and use logger to print
        std::cerr << "Error reading spell json file " << filePath << std::endl;
        std::cerr << e.what() << std::endl;
        return false;
      } //end  catch (std::exception e)

      return true;
    }
    bool Feat::saveToFile(const std::string& filePath)
    {
      std::ofstream f(filePath);
      nlohmann::json j;
      j["feat"] = *this;

      try
      {
        f << j.dump(4);
      }
      catch(std::exception e)
      {
        //TODO actuall catch real exception and use logger to print
        std::cerr << "Error writing spell json file " << filePath << std::endl;
        std::cerr << e.what() << std::endl;
        return false;
      }

      return true;
    }

    //comparison operators
    bool operator==(const Feat& lhs, const Feat& rhs)
    {
      return lhs.name == rhs.name &&
             lhs.prerequisites == rhs.prerequisites &&
             lhs.flavorText == rhs.flavorText &&
             lhs.abilities == rhs.abilities;
    }
    bool operator!=(const Feat& lhs, const Feat& rhs)
    {
      return !(lhs == rhs);
    }

    //ostream operators
    std::ostream& operator<<(std::ostream& out, const Feat& f)
    {
      nlohmann::json j;
      j["feat"] = f;
      out << j.dump(4);
      return out;
    }
  }
}
