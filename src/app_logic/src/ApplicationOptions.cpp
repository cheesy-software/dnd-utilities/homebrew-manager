#include "ApplicationOptions.h"

#include <unordered_map>
#include <algorithm>

namespace CheesySoftware
{
  namespace HomebrewManager
  {
      Editor AppOptions::editorFromStr(const std::string& str)
      {
        //reverse lookup map
        static std::unordered_map<std::string, Editor> strMap = {
          { "class",     Editor::CLASS },
          { "encounter", Editor::ENCOUNTER },
          { "feat",      Editor::FEAT },
          { "item",      Editor::ITEM },
          { "monster",   Editor::MONSTER },
          { "npc",       Editor::NPC },
          { "race",      Editor::RACE },
          { "spell",     Editor::SPELL },
        };

        //make string lowercase
        auto strLower = str;
        std::transform(strLower.begin(), strLower.end(), strLower.begin(), [] (unsigned char c) {
            return std::tolower(c);
          });

        //if string is not in map, return none
        auto itr = strMap.find(strLower);
        if (itr == strMap.end())
        {
          return Editor::NONE;
        }

        return itr->second;
      }
  }
}

using CheesySoftware::HomebrewManager::Verbosity;

//output stream operator for verbosity
std::ostream& operator<<(std::ostream& out, const Verbosity& toPrint)
{
  switch (toPrint)
  {
    case Verbosity::ALL_INFO:
      out << "ALL_INFO";
      break;
    case Verbosity::DEBUG_INFO:
      out << "DEBUG_INFO";
      break;
    case Verbosity::ERROR:
      out << "ERROR";
      break;
    case Verbosity::FUNCTION_NAME:
      out << "FUNCTION_NAME";
      break;
    case Verbosity::INFO:
      out << "INFO";
      break;
    case Verbosity::NO_INFO:
      out << "NO_INFO";
      break;
    case Verbosity::VERBOSE_INFO:
      out << "VERBOSE_INFO";
      break;
    case Verbosity::WARNING:
      out << "WARNING";
      break;
  } //end  switch (toPrint)
  return out;
}

