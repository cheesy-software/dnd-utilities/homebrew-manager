#include "Monster.h"

#include <fstream>

#include "json.hpp"

namespace CheesySoftware
{
  namespace HomebrewManager
  {
    const Monster::Version Monster::VERSION = Monster::Version(1, 0);
    const std::map<Monster::Size, std::string> Monster::SIZE_STR_MAP = {
      {Size::TINY,       "Tiny"},
      {Size::SMALL,      "Small"},
      {Size::MEDIUM,     "Medium"},
      {Size::LARGE,      "Large"},
      {Size::HUGE,       "Huge"},
      {Size::GARGANTUAN, "Gargantuan"},
      {Size::VARIES,     "Varies"}
    };
    const std::map<Monster::Type, std::string> Monster::TYPE_STR_MAP = {
      {Type::ABERRATION,  "Aberration"},
      {Type::BEAST,       "Beast"},
      {Type::CELESTIAL,   "Celestial"},
      {Type::CONSTRUCT,   "Construct"},
      {Type::DRAGON,      "Dragon"},
      {Type::ELEMENTAL,   "Elemental"},
      {Type::FEY,         "Fey"},
      {Type::FIEND,       "Fiend"},
      {Type::GIANT,       "Giant"},
      {Type::HUMANOID,    "Humanoid"},
      {Type::MONSTROSITY, "Monstrosity"},
      {Type::OOZE,        "Ooze"},
      {Type::PLANT,       "Plant"},
      {Type::UNDEAD,      "Undead"}
    };
    const std::map<Monster::Alignment, std::string> Monster::ALIGNMENT_STR_MAP {
      {Alignment::LAWFUL_GOOD,     "Lawful Good"},
      {Alignment::NEUTRAL_GOOD,    "Neutral Good"},
      {Alignment::CHAOTIC_GOOD,    "Chaotic Good"},
      {Alignment::LAWFUL_NEUTRAL,  "Lawful Neutral"},
      {Alignment::TRUE_NEUTRAL,    "True Neutral"},
      {Alignment::CHAOTIC_NEUTRAL, "Chaotic Neutral"},
      {Alignment::LAWFUL_EVIL,     "Lawful Evil"},
      {Alignment::NEUTRAL_EVIL,    "Neutral Evil"},
      {Alignment::CHAOTIC_EVIL,    "Chaotic Evil"}
    };
    const std::map<Monster::Stat, std::string> Monster::STAT_STR_MAP {
      {Stat::STRENGTH,     "Strength"},
      {Stat::DEXTERITY,    "Dexterity"},
      {Stat::CONSTITUTION, "Constitution"},
      {Stat::INTELLIGENCE, "Intelligence"},
      {Stat::WISDOM,       "Wisdom"},
      {Stat::CHARISMA,     "Charisma"}
    };
    const std::map<Monster::Skill, std::string> Monster::SKILL_STR_MAP {
      {Skill::ACROBATICS,      "Acrobatics"},
      {Skill::ANIMAL_HANDLING, "Animal Handling"},
      {Skill::ARCANA,          "Arcana"},
      {Skill::ATHLETICS,       "Athletics"},
      {Skill::DECEPTION,       "Deception"},
      {Skill::HISTORY,         "History"},
      {Skill::INSIGHT,         "Insight"},
      {Skill::INTIMIDATION,    "Intimidation"},
      {Skill::INVESTIGATION,   "Investigation"},
      {Skill::MEDICINE,        "Medicine"},
      {Skill::NATURE,          "Nature"},
      {Skill::PERCEPTION,      "Perception"},
      {Skill::PERFORMANCE,     "Performance"},
      {Skill::PERSUASION,      "Persuasion"},
      {Skill::RELIGION,        "Religion"},
      {Skill::SLEIGHT_OF_HAND, "Sleight Of Hand"},
      {Skill::STEALTH,         "Stealth"},
      {Skill::SURVIVAL,        "Survival"}
    };
    const std::map<Monster::Skill, Monster::Stat> Monster::SKILL_STAT_MAP = {
      {Skill::ACROBATICS,      Stat::DEXTERITY},
      {Skill::ANIMAL_HANDLING, Stat::WISDOM},
      {Skill::ARCANA,          Stat::INTELLIGENCE},
      {Skill::ATHLETICS,       Stat::STRENGTH},
      {Skill::DECEPTION,       Stat::CHARISMA},
      {Skill::HISTORY,         Stat::INTELLIGENCE},
      {Skill::INSIGHT,         Stat::WISDOM},
      {Skill::INTIMIDATION,    Stat::CHARISMA},
      {Skill::INVESTIGATION,   Stat::INTELLIGENCE},
      {Skill::MEDICINE,        Stat::WISDOM},
      {Skill::NATURE,          Stat::INTELLIGENCE},
      {Skill::PERCEPTION,      Stat::WISDOM},
      {Skill::PERFORMANCE,     Stat::CHARISMA},
      {Skill::PERSUASION,      Stat::CHARISMA},
      {Skill::RELIGION,        Stat::INTELLIGENCE},
      {Skill::SLEIGHT_OF_HAND, Stat::DEXTERITY},
      {Skill::STEALTH,         Stat::DEXTERITY},
      {Skill::SURVIVAL,        Stat::WISDOM}
    };

    void to_json(nlohmann::json& j, const Monster::ModifierInfo& m)
    {
      j = {
        {"autoCalculate", m.autoCalculate},
        {"proficient", m.proficient},
        {"modifier", m.modifier}
      };
    }
    void from_json(const nlohmann::json& j, Monster::ModifierInfo& m)
    {
      j.at("autoCalculate").get_to(m.autoCalculate);
      j.at("proficient").get_to(m.proficient);
      j.at("modifier").get_to(m.modifier);
    }

    void to_json(nlohmann::json& j, const Monster::Version& v)
    {
      j = {
        {"major", v.major},
        {"minor", v.minor}
      };
    }
    void from_json(const nlohmann::json& j, Monster::Version& v)
    {
      j.at("major").get_to(v.major);
      j.at("minor").get_to(v.minor);
    }

    void to_json(nlohmann::json& j, const Monster& m)
    {
      j = {
        {"version", m.VERSION},
        {"name", m.name},
        {"size", m.size},
        {"type", m.type},
        {"alignment", m.alignment},
        {"ac", m.ac},
        {"acReason", m.acReason},
        {"hp", m.hp},
        {"hpDice", m.hpDice},
        {"speed", m.speed},
        {"senses", m.senses},
        {"languages", m.languages},
        {"challengeRating", m.challengeRating},
        {"xp", m.xp},
        {"stats", m.stats},
        {"savingThrows", m.savingThrows},
        {"skills", m.skills},
        {"abilities", m.abilities},
        {"actions", m.actions},
        {"info", m.info},
        {"proficiencyBonus", m.proficiencyBonus},
        {"damageResistances", m.damageResistances},
        {"damageImmunities", m.damageImmunities},
        {"damageVulnerabilities", m.damageVulnerabilities},
        {"conditionImmunities", m.conditionImmunities},
      };
    }
    void from_json(const nlohmann::json& j, Monster& m)
    {
        j.at("name").get_to(m.name);
        j.at("size").get_to(m.size);
        j.at("type").get_to(m.type);
        j.at("alignment").get_to(m.alignment);
        j.at("ac").get_to(m.ac);
        j.at("acReason").get_to(m.acReason);
        j.at("hp").get_to(m.hp);
        j.at("hpDice").get_to(m.hpDice);
        j.at("speed").get_to(m.speed);
        j.at("senses").get_to(m.senses);
        j.at("languages").get_to(m.languages);
        j.at("challengeRating").get_to(m.challengeRating);
        j.at("xp").get_to(m.xp);
        j.at("stats").get_to(m.stats);
        j.at("savingThrows").get_to(m.savingThrows);
        j.at("skills").get_to(m.skills);
        j.at("abilities").get_to(m.abilities);
        j.at("actions").get_to(m.actions);
        j.at("info").get_to(m.info);
        j.at("proficiencyBonus").get_to(m.proficiencyBonus);
        j.at("damageResistances").get_to(m.damageResistances);
        j.at("damageImmunities").get_to(m.damageImmunities);
        j.at("damageVulnerabilities").get_to(m.damageVulnerabilities);
        j.at("conditionImmunities").get_to(m.conditionImmunities);
    }

    bool Monster::loadFromFile(const std::string& filePath)
    {
      nlohmann::json j;
      std::ifstream f(filePath);

      try
      {
        f >> j;
        j.at("monster").get_to(*this);
      } //end  try
      catch (std::exception e)
      {
        //TODO actually catch real exception and use logger to print
        std::cerr << "Error reading monster json file " << filePath << std::endl;
        std::cerr << e.what() << std::endl;
        return false;
      } //end  catch (std::exception e)

      return true;
    }
    bool Monster::saveToFile(const std::string& filePath)
    {
      std::ofstream f(filePath);
      nlohmann::json j;
      j["monster"] = *this;

      try
      {
        f << j.dump(4);
      }
      catch(std::exception e)
      {
        //TODO actuall catch real exception and use logger to print
        std::cerr << "Error writing monster json file " << filePath << std::endl;
        std::cerr << e.what() << std::endl;
        return false;
      }

      return true;
    }

    //comparison operators
    bool operator==(const Monster::ModifierInfo& lhs, const Monster::ModifierInfo& rhs)
    {
      return lhs.autoCalculate == rhs.autoCalculate &&
             lhs.proficient == rhs.proficient &&
             lhs.modifier == rhs.modifier;
    }
    bool operator!=(const Monster::ModifierInfo& lhs, const Monster::ModifierInfo& rhs)
    {
      return !(lhs == rhs);
    }
    bool operator==(const Monster& lhs, const Monster& rhs)
    {
      return lhs.name == rhs.name &&
             lhs.size == rhs.size &&
             lhs.type == rhs.type &&
             lhs.alignment == rhs.alignment &&
             lhs.ac == rhs.ac &&
             lhs.acReason == rhs.acReason &&
             lhs.hp == rhs.hp &&
             lhs.hpDice == rhs.hpDice &&
             lhs.speed == rhs.speed &&
             lhs.senses == rhs.senses &&
             lhs.languages == rhs.languages &&
             lhs.challengeRating == rhs.challengeRating &&
             lhs.xp == rhs.xp &&
             lhs.stats == rhs.stats &&
             lhs.savingThrows == rhs.savingThrows &&
             lhs.skills == rhs.skills &&
             lhs.abilities == rhs.abilities &&
             lhs.actions == rhs.actions &&
             lhs.info == rhs.info &&
             lhs.proficiencyBonus == rhs.proficiencyBonus &&
             lhs.damageResistances == rhs.damageResistances &&
             lhs.damageImmunities == rhs.damageImmunities &&
             lhs.damageVulnerabilities == rhs.damageVulnerabilities &&
             lhs.conditionImmunities == rhs.conditionImmunities;
    }
    bool operator!=(const Monster& lhs, const Monster& rhs)
    {
      return !(lhs == rhs);
    }

    //ostream operators
    std::ostream& operator<<(std::ostream& out, const Monster::ModifierInfo& m)
    {
      nlohmann::json j;
      j["modifier_info"] = m;
      out << j.dump(4);
      return out;
    }
    std::ostream& operator<<(std::ostream& out, const Monster& m)
    {
      nlohmann::json j;
      j["monster"] = m;
      out << j.dump(4);
      return out;
    }
  }
}
